(function ($) {

Drupal.superslidetabsShowHide = function() {
  $(this).parents('tr').find('div.st-tab-' + this.value + '-options-form').show().siblings('div.st-tab-options-form').hide();
};

Drupal.behaviors.quicktabsform = {
  attach: function (context, settings) {
    $('#superslide-tabs tr').once(function(){
      var currentRow = $(this);
      currentRow.find('div.form-item :input[name*="type"]').bind('click', Drupal.superslidetabsShowHide);
      $(':input[name*="type"]:checked', this).trigger('click');
    })
  }
};

})(jQuery);
