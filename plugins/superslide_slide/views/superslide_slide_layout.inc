<?php
/**
 * @file
 * Definition of superslide_plugin_style.
 */

/**
 * Class to define a style plugin handler.
 */
class SuperslideSlideLayout extends views_plugin_style {
  /**
   * Definition.
   */
  public function option_definition() {
    $options = parent::option_definition();
    // Superslide option definition.
    $options['superslide'] = array(
      'contains' => array(        
        'enable' => array('default' => TRUE, 'bool' => TRUE),
        'options' => array(
          'type' => array('default' => ''),
          'effect' => array('default' => ''),
        ),        
      ),
    );
    $options['superslide_slide'] = array(
      'mappings' => array(
        0 => array(
          'target' => 'node_id',
          'source' => '',
        ),
        1 => array(
          'target' => 'slide_image',
          'source' => '',
        ),
        2 => array(
          'target' => 'slide_thumbnail',
          'source' => '',
        ),
        3 => array(
          'target' => 'slide_title',
          'source' => '',
        ),
        4 => array(
          'target' => 'slide_subtitle',
          'source' => '',
        ),      
        5 => array(
          'target' => 'slide_text',
          'source' => '',
        ),
        6 => array(
          'target' => 'slide_path',
          'source' => '',
        ),
      ),
    );
    return $options;
  }

  /**
   * Form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    //Load css
    drupal_add_css(drupal_get_path('module', 'superslide') . '/css/superslide.css');

    // get the possible source fields form the view
    $source_fields = array('' => t('<None>'));
    if ($this->uses_fields()) {
      foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
        $format = isset($handler->options['format']) ? ' - ' . $handler->options['format'] : '';
        if ($label = $handler->label()) {
          $source_fields[$field] = $label . $format;
        }
        else {
          $source_fields[$field] = $handler->ui_name() . $format;
        }
      }
    }

    // Wrapper in which to place both the mapped fields and the 'add mapping' form.
    $form['superslide_slide'] = array(
      '#tree' => TRUE,
      '#weight' => -5,
      '#prefix' => '<div class="clear-block" id="views-slideshow-superslide-mapping-wrapper">',
      '#suffix' => '</div>',
    );

    // Wrapper in which to place both the mapped fields and the 'add mapping' form.
    $form['superslide_slide']['helptext'] = array(
      '#type' => 'item',
      '#title' => t('Field mappings'),
      '#description' => t('In this part you map available fields to superslide theme fields.<br />The superslide theme fields are the target.<br />Source fields are fields available from your view.'),
    );

    // Wrapper to display existing mappingss.
    $form['superslide_slide']['mappings'] = array(
      '#tree' => TRUE,
      '#prefix' => '<div id="views-slideshow-superslide-mappings">',
      '#suffix' => '</div>',
      '#theme' => 'superslide_slide_mappings_table',
    );

    $mappings = array(
      0 => array(
        'target' => 'node_id',
        'source' => $this->options['superslide_slide']['mappings'][0]['source'] ? $this->options['superslide_slide']['mappings'][0]['source'] : '',
      ),
      1 => array(
        'target' => 'slide_image',
        'source' => $this->options['superslide_slide']['mappings'][1]['source'] ? $this->options['superslide_slide']['mappings'][1]['source'] : '',
      ),
      2 => array(
        'target' => 'slide_thumbnail',
        'source' => $this->options['superslide_slide']['mappings'][2]['source'] ? $this->options['superslide_slide']['mappings'][2]['source'] : '',
      ),
      3 => array(
        'target' => 'slide_title',
        'source' => $this->options['superslide_slide']['mappings'][3]['source'] ? $this->options['superslide_slide']['mappings'][3]['source'] : '',
      ),
      4 => array(
        'target' => 'slide_subtitle',
        'source' => $this->options['superslide_slide']['mappings'][4]['source'] ? $this->options['superslide_slide']['mappings'][4]['source'] : '',
      ),
      5 => array(
        'target' => 'slide_text',
        'source' => $this->options['superslide_slide']['mappings'][5]['source'] ? $this->options['superslide_slide']['mappings'][5]['source'] : '',
      ),
      6 => array(
        'target' => 'slide_path',
        'source' => $this->options['superslide_slide']['mappings'][6]['source'] ? $this->options['superslide_slide']['mappings'][6]['source'] : '',
      ), 
    );

    // Get number of mappings.
    $mapping_count = empty($mappings) ? 0 : count($mappings);
    // Add the existing mappings to the form.
    for ($delta = 0; $delta < $mapping_count; $delta++) {
      $target = isset($mappings[$delta]['target']) ? $mappings[$delta]['target'] : '';
      $source = isset($mappings[$delta]['source']) ? $mappings[$delta]['source'] : '';
      // Display existing mappings using helper function superslide_slide_mapping_display_form().
      $form['superslide_slide']['mappings'][$delta] = superslide_slide_mapping_display_form($delta, $target, $source, $source_fields);
    }

    $form['superslide'] = array(
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('views-attachment', 'fieldset-no-legend'),),
      '#tree' => TRUE,
    );   

    $form['superslide']['enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable SuperSlide'),
      '#description' => t('If you enable this, you can add a special effects for this output.'),
      '#default_value' => $this->options['superslide']['enable'],
      '#id' => 'edit-enable-superslide',
    );
    $form['superslide']['options'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('options-set'),),
      '#dependency' => array(
        'edit-enable-superslide' => array(1),
      ),
      '#pre_render' => array('ctools_dependent_pre_render'),
      '#prefix' => '<div id="edit-options-wrapper">',
      '#suffix' => '</div>',
    );
    $form['superslide']['options']['type'] = array(
      '#type' => 'select',
      '#title' => 'Type of Superslide',
      '#options' => drupal_map_assoc(array('superslide_slide')),
      '#default_value' => $this->options['superslide']['options']['type'],
    );    
    $form['superslide']['options']['superslide_slide'] = array(
      '#type' => 'select',
      '#title' =>t("@name Effect", array('@name' => $type)),
      '#options' => superslide_get_effects_by_type('superslide_slide'),
      '#default_value' => $this->options['superslide']['options']['superslide_slide'] ? $this->options['superslide']['options']['superslide_slide'] : '-none-',
    );
  }
}
