<?php

/**
 * Implementation of template preprocess for the view.
 */
function template_preprocess_superslide_slide_layout(&$vars) {
  $view = $vars['view'];
  $rows = $vars['rows'];

  $all_types = superslide_types_list_all();
  $effect_type = $vars['options']['superslide']['options']['type'];
  $effect = superslide_get_effects($vars['options']['superslide']['options'][$effect_type]);
  $effect_css = _superslide_dir_scan_file($effect->type, $effect->layout_template, '.css');
  $settings = array(
    'effect_template' => $effect->layout_template,
  );
  if (!empty($rows)){
    $rows_count = count($rows);
    for ($delta = 0; $delta < $rows_count; $delta++) {
      foreach($vars['options']['superslide_slide']['mappings'] as $id => $field){
        if(!empty($field['source'])){
          $results[$delta][$field['target']] = $view->render_field($field['source'], $delta);
        }      
      }
    }
  }

  $vars['slides'] = theme('superslide_layout', array('settings' => $settings, 'contents' => $results));

  if(!empty($effect_type)){
    drupal_add_js(drupal_get_path('module', 'superslide') . '/js/jquery.superslide.js');     
    drupal_add_css($effect_css);
  }
}

