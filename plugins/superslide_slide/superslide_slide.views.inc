<?php
/**
 * @file
 * Provide views data and handlers.
 */

/**
 * Implements hook_views_plugins().
 */
function superslide_slide_views_plugins() {
  $module_path = drupal_get_path('module', 'superslide_slide');

  return array(
    'style' => array(
      'superslide_slide_layout' => array(
        'title' => t('Superslide Slide'),
        'path' => $module_path . '/views',
        'handler' => 'SuperslideSlideLayout',
        'parent' => 'default',
        'theme' => 'superslide_slide_layout',
        'theme path' => $module_path . '/views/templates',
        'theme file' => 'superslide_slide.theme.inc',
        'uses row plugin' => TRUE,
        'uses grouping' => FALSE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),      
    ),
  );
}
