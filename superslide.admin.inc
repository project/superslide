<?php

/**
 * @file
 *  admin effects of the superslide module.
 *
 */

/**
 * Form with overview of all superslide effect.
 */
function superslide_effect_add_form($form, &$form_state) {
  // Load superslide css and js.
  drupal_add_css(drupal_get_path('module', 'superslide') . '/css/superslide.css');

  if(!_superslide_check_loyout_dir()){
    drupal_set_message(t('There no any layout template. plese go to <a href="@url" target="_blank">Download</a> template.', array('@url' => url("http://www.drupalhunter.com/superslide.html"))), 'error');
  }

  // get superslide effect.
  $rows = array();
  $effects = superslide_get_effects(NULL);

  foreach ($effects as $effect) {
    $base_rows = array(
      'name' => check_plain($effect->name),
      'type' => $effect->type,
      'layout' => $effect->layout_template,
      'edit' => l(t('Edit'), "admin/structure/speffect/edit/$effect->sid"),
      'delete' => l(t('Delete'), "admin/structure/speffect/delete/$effect->sid"),      
    );
    if($effect->type == 'superslide_slide' || $effect->type == 'superslide_carousel') {      
      $rows[] = $base_rows+array(
        'content' => '',
      );
    }else{
      $rows[] = $base_rows+array(
        'content' => l(t('Configure contents'), "admin/structure/speffect/$effect->sid/contents"),
      );
    }
  }
  if (empty($rows)) {
    $rows[] = array(array(
        'data' => t('No superslide effect available.'),
        'colspan' => '4',
      ));
  }

  $header = array(t('Name'), t('Type'), array(
      'data' => t('Operation'),
      'colspan' => '4',
    ));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'superslide')));

  $form = array();
  $form['list'] = array(
    '#type' => 'fieldset',
    '#title' => t('superslide effect library'),
    '#collapsible' => TRUE,
  );
  $form['list']['table'] = array(
    '#type' => 'item',
    '#prefix' => '<div>',
    '#markup' => $output,
    '#suffix' => '</div>',
  );

  // add superslide effect.
  $form['effect'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add superslide effect.'),
  );

  $form['effect']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect name'),
    '#description' => t('A effect with this same name will be created.'),
    '#default_value' => '',
    '#required' => TRUE,
  );

  $all_types = superslide_get_types();
  if(!empty($all_types)){
    $form['effect']['type'] = array(
      '#type' => 'select',
      '#title' => t('Type of superslide effect'),
      '#options' => superslide_get_types(),
      '#required' => TRUE,
      '#ajax' => array(
        'callback' => 'superslide_layout_select_ajax_callback',
        'wrapper' => 'layout-templates',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
  }

  $form['effect']['skin']['layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout Setting'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attributes' => array('class' => array('effect-layout')),
  );

  $options = array();

  if( (isset($form_state['values']['type'])) && ($form_state['values']['type'] != '') ) {
    $effect_type = $form_state['values']['type'];    
  }else{
    $effect_type = '';
  }

  $options = superslide_get_layout_templates($effect_type);

  $form['effect']['skin']['layout']['template'] = array(
    '#theme' => 'superslide_layout_select_form',
    '#type' => 'radios',
    '#title' => t('Select a layout'),
    '#options' => $options,
    '#multiple' => FALSE,
    '#required' => TRUE,
    '#prefix' => '<div id="layout-templates">',
    '#suffix' => '</div>',
    '#attributes' => array('data-effect-type' => $effect_type),
    '#description' => t("Available superslide effect themes are automatically detected form the folder [Your Theme]/superslide_effects/."),
  );
  
  $form['effect']['asblock'] = array(
    '#type' => 'checkbox',
    '#title' => t('As a block'),
    '#default_value' => TRUE,
    '#description'   => t('If checked, it will auto generate a block.'),
    '#states' => array(
      'visible' => array(
        'select[name="type"]' => array('value' => 'superslide_menu')
      ),
    ),
  );

  $form['effect']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Add effect'),
  );

  return $form;
}

/**
 * Ajax callback for layout select.
 */
function superslide_layout_select_ajax_callback($form, $form_state) {
  if( (isset($form_state['values']['type'])) && ($form_state['values']['type'] != '') ) {
    $effec_ttype = $form_state['values']['type'];
    $form['effect']['skin']['layout']['template']['#options'] = superslide_get_layout_templates($effec_ttype);
    return $form['effect']['skin']['layout']['template'];
  }
}

/**
 * Get superslide layout templates options.
 */
function superslide_get_layout_templates($effec_ttype) {
  $files = _superslide_dir_scan_directory($effec_ttype, '.config');
    
  if (!empty($files)) {
    asort($files);
    
    foreach ($files as $key => $file) {
      $options[$key] = '<li class="template-item"><img src="'.base_path() . _superslide_dir_scan_file($key, 'screenshot.png').'" /><span>'.$key.'</span></li>';
    }

    return $options;
  }
}

/**
 * Returns HTML for a theme's superslide layout select form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_superslide_layout_select_form($variables) {
  $form = $variables['form'];

  $output  = '';
  $output .= '<ul class="template-list">';
  // layout template options
  if(!empty($form['#options'])){
    $output .= drupal_render_children($form);;
  }
  $output .= '</ul>';

  return $output;
}

/**
 * Validate "Add Effect" form.
 */
function superslide_effect_add_form_validate($form, &$form_state) {
  $effects = superslide_get_effects(NULL);
  $effect_name = array();
  foreach ($effects as $effect) {
    $effect_name[] = $effect->name;
  }

  if (!empty($effect_name)) {
    // Check if name is unique
    if (in_array($form_state['values']['name'], $effect_name)) {
      form_set_error('', t('Superslide effect %s already exists. Please use a different name.', array('%s' => $form_state['values']['name'])));
    }
  }

  if ($form_state['values']['type'] == 'superslide_slide') {
    $form_state['values']['asblock'] = 0;
  }
}
/**
 * Add superslide effect to database from "Add Effect" form.
 */
function superslide_effect_add_form_submit($form, &$form_state) {
  $id = db_insert('superslide_effects')
  ->fields(array(
    'name' => $form_state['values']['name'],
    'type' => $form_state['values']['type'],
    'layout_template' => $form_state['values']['template'],
    'asblock' => $form_state['values']['asblock'],
  ))
  ->execute();

  drupal_set_message(t('Superslide effect %s added. please congif it.', array('%s' => $form_state['values']['name'])));
}

/**
 * Edit effect form.
 */
function superslide_effect_edit_form($form, &$form_state, $sid) {
  // Load superslide css and js.
  drupal_add_css(drupal_get_path('module', 'superslide') . '/css/superslide.css');

  if(!_superslide_check_loyout_dir()){
    drupal_set_message(t('There no any layout template. plese go to <a href="@url" target="_blank">Download</a> template.', array('@url' => url("http://www.drupalhunter.com/superslide.html"))), 'error');
  }

  $effect = superslide_get_effects($sid);

  $form = array();

  $form['sid'] = array(
    '#type' => 'hidden',
    '#value' => $sid,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect name'),
    '#description' => t('The effect name must be unique.'),
    '#default_value' => $effect->name,
    '#required' => TRUE,
  );

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type of superslide effect'),
    '#default_value' => $effect->type,
    '#options' => superslide_get_types(),
    '#required' => TRUE,
  );

  $options = array();

  if( (isset($form_state['values']['type'])) && ($form_state['values']['type'] != '') ) {
    $effect_type = $form_state['values']['type'];    
  }else{
    $effect_type = $effect->type;
  }

  $options = superslide_get_layout_templates($effect_type);

  $form['effect']['skin']['layout']['template'] = array(
    '#theme' => 'superslide_layout_select_form',
    '#type' => 'radios',
    '#title' => t('Select a layout'),
    '#default_value' => $effect->layout_template,
    '#options' => $options,
    '#multiple' => FALSE,
    '#required' => TRUE,
    '#prefix' => '<div id="layout-templates">',
    '#suffix' => '</div>',
    '#attributes' => array('data-effect-type' => $effect_type),
    '#description' => t("Available superslide effect themes are automatically detected form the folder [Your Theme]/superslide_effects/."),
  );

  $form['asblock'] = array(
    '#type' => 'checkbox',
    '#title' => t('As a block'),
    '#default_value' => $effect->asblock,
    '#description'   => t('If checked, it will auto generate a block.'),
    '#states' => array(
      'visible' => array(
        'select[name="type"]' => array('value' => 'superslide_menu'),
      ),
    ),
  );

  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate edit effect form.
 */
function superslide_effect_edit_form_validate($form, &$form_state) {
  $effects = superslide_get_effects(NULL);
  $effect_name = array();
  foreach ($effects as $effect) {
    $effect_name[$effect->sid] = $effect->name;
  }

  // Remove current effect name to prevent false error.
  unset($effect_name[$form_state['values']['sid']]);

  if (!empty($effect_name)) {
    // Check if name is unique.
    if (in_array($form_state['values']['name'], $effect_name)) {
      form_set_error('', t('Superslide effect %s already exists. Please use a different name.', array('%s' => $form_state['values']['name'])));
    }
  }
}

/**
 * Submit edit effect form.
 */
function superslide_effect_edit_form_submit($form, &$form_state) {
  db_update('superslide_effects')
  ->fields(array(
    'name' => $form_state['values']['name'],
    'type' => $form_state['values']['type'],
    'layout_template' => $form_state['values']['template'],
    'asblock' => $form_state['values']['asblock'],
  ))
  ->condition('sid', $form_state['values']['sid'])
  ->execute();

  drupal_set_message(t('Superslide effect updated.'));
  $form_state['redirect'] = 'admin/structure/superslide';
  return $form;
}

/**
 * Delete effect form.
 */
function superslide_effect_confirm_delete_form($form, &$form_state, $sid) {
  $effect = superslide_get_effects($sid);
  if (empty($effect)) {
    drupal_set_message(t('The effect with sid @sid was not found.', array('@sid' => $sid)), 'error');
    return array();
  }
  else {
    if ($effect->enabled == 1) {

      //The question to ask the user.
      $question = t('Are you sure you want to delete the superslide effect %name?', array('%name' => $effect->name));
    }
    else {
      //The question to ask the user.
      $question = t('Are you sure you want to delete the superslide effect %name?', array('%name' => $effect->name));
    }
    // The page to go to if the user denies the action.
    $path = 'admin/structure/superslide';

    // Additional text to display (defaults to "This action cannot be undone.").
    $description = t('This action cannot be undone.');

    // A caption for the button which confirms the action.
    $yes = t('Delete');

    // A caption for the link which denies the action.
    $no = t('Cancel');

    // set sid value to use in submit function.
    $form['sid'] = array(
      '#type' => 'value',
      '#value' => $sid,
    );

    // set the redirect path value to use in submit function.
    $form_state['redirect'] = $path;
    return confirm_form(
      $form,
      $question,
      $path,
      $description,
      $yes,
      $no
    );
  }
}

/**
 * Delete a superslide effect.
 */
function superslide_effect_confirm_delete_form_submit($form, &$form_state) {

  if (superslide_effect_confirm_delete($form_state['values']['sid'])) {
    $succes_message = t('Superslide effect successfully deleted!');
    drupal_set_message($succes_message);
  }
  else {
    $failure_message = t('There was a problem deleting the superslide effect');
    drupal_set_message($failure_message);
  }
  $form_state['redirect'] = 'admin/structure/superslide';
  return $form;
}

/**
 * Delete a superslide effect from the database.
 */
function superslide_effect_confirm_delete($sid) {
  $result = db_delete('superslide_effects')
  ->condition('sid', (int) $sid)
  ->execute();

  return TRUE;
}

/**
 * Config effect form.
 */
function superslide_contents_config_form($form, &$form_state, $sid) {
  $effect = superslide_get_effects($sid); 
  
  drupal_add_css(drupal_get_path('module', 'superslide') . '/css/superslide.css');

  $form = array();

  $form['back_to_list'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="backlist">' .l('Back List', 'admin/structure/superslide/list'). '</div>',
  );

  if($effect->type == 'superslide_menu'){
    if($effect->asblock == 1){
      drupal_goto('admin/structure/block/manage/superslide/'.$effect->sid.'/configure');
    }else{
      return 'No config options';
    }    
  }
  
  if($effect->type == 'superslide_tabs' && module_exists('superslide_tabs')){
    $sptabs = superslide_tabs_get_tabs($sid);

    if(!empty($sptabs)){
      $effect->tabs = unserialize($sptabs->tabs);
    }

    $form['sp_wrapper']['tabs'] = array(
      '#tree' => TRUE,
      '#prefix' => '<div id="superslide-tabs">',
      '#suffix' => '</div>',
      '#theme' => 'superslide_tabs_admin_form_tabs',
    );
    // If creating a new superslide tabs instance, start off with 2 empty tabs.
    if (empty($effect->tabs)) {
      $effect->tabs = array(
        0 => array(),
        1 => array(),
      );
    }
    // If the "Add another" button was clicked, we need to increment the number of
    // tabs by one.
    if (isset($form_state['num_tabs']) && $form_state['num_tabs'] > count($effect->tabs)) {
      $effect->tabs[] = array();
    }
    $form_state['num_tabs'] = count($effect->tabs);
    
    // If the "Remove" button was clicked for a tab, we need to remove that tab
    // from the form.
    if (isset($form_state['to_remove'])) {
      unset($effect->tabs[$form_state['to_remove']]);
      unset($form_state['to_remove']);
      $form_state['num_tabs']--;
    }

    $tab_titles = array(
      SUPERSLIDE_TABS_DELTA_NONE => t('- None -'),
    );
    
    // Add current tabs to the form.
    foreach ($effect->tabs as $delta => $tab) {
      $tab['delta'] = $delta;
      $form['sp_wrapper']['tabs'][$delta] = _superslide_tabs_form($tab, $effect);
      if (isset($tab['title'])) {
        $tab_titles[$delta] = $tab['title'];
      }
    }

    // If there's only one tab, it shouldn't be removeable.
    if (count($effect->tabs) == 1) $form['sp_wrapper']['tabs'][$delta]['remove']['#access'] = FALSE;

    $form['sp_wrapper']['tabs_more'] = array(
      '#type' => 'submit',
      '#prefix' => '<div id="add-more-tabs-button">',
      '#suffix' => '<label for="edit-tabs-more">' . t('Add tab') . '</label></div>',
      '#value' => t('More tabs'),
      '#attributes' => array('class' => array('add-tab'), 'title' => t('Click here to add more tabs.')),
      '#weight' => 1,
      '#submit' => array('superslide_tabs_more_tabs_submit'),
      '#ajax' => array(
        'callback' => 'superslide_tabs_ajax_callback',
        'wrapper' => 'superslide-tabs',
        'effect' => 'fade',
      ),
      '#limit_validation_errors' => array(),
    );
    $form['#validate'][] = 'superslide_tabs_form_validate';
  }

  $form['sid'] = array(
    '#type' => 'hidden',
    '#value' => $sid,
  );

  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit config contents form.
 */
function superslide_contents_config_form_submit($form, &$form_state) {
   if(!empty($form_state['values']['sid'])){    
    $st = _superslide_tabs_convert_form_to_sptabs($form_state);

    $sptabs = superslide_tabs_get_tabs($form_state['values']['sid']);
    if ($sptabs) {
      $ret = drupal_write_record('superslide_tabs', $st, 'sid');
      if ($ret == SAVED_UPDATED) {
        drupal_set_message(t('The superslide tabs content has been updated.'));
      }
    }else{
      $ret = drupal_write_record('superslide_tabs', $st);
      if ($ret == SAVED_NEW) {
        drupal_set_message(t('The superslide tabs content has been created.'));
      }
    }

    $form_state['redirect'] = 'admin/structure/superslide';
  }
}

/**
 * superslide settings form.
 */
function superslide_settings_form($form, &$form_state) {
  $form = array();

  $form['superslide_todo'] = array(
    '#type' => 'markup',
    '#title' => t('TO DO'),
    '#markup' => t('TO DO : Here is some global config for superslide'),
  );

  $form_state['redirect'] = 'admin/structure/superslide/list';

  return system_settings_form($form);
}
