<?php

/**
 * @file
 * Super Slide API documentation.
 */

/**
 * hook_superslide_types_info()
 * Declare superslide_types
 *
 * @return
 *     An associative array of shortcodes, whose keys are internal shortcode names,
 *     which should be unique..
 *     Each value is an associative array describing the shortcode, with the
 *     following elements (all are optional except as noted):
 *   - name: (required) An administrative summary of what the shortcode does.
 *   - description: Additional administrative information about the shortcode's
 *     behavior, if needed for clarification.
 *
 */
function hook_superslide_types_info() {
  $superslide_types['superslide_slide'] = array(
    'name' => t('superslide slide'),
    'description' => t('superslide slide effect.'),
  );

  return $superslide_types;
}
